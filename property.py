# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=too-few-public-methods

"""
Common properties with command descriptions, bit-masks and enums
"""

from enum import Enum, IntEnum

COMMAND_CODE_MASK: int = 0b11110000000000000000000000000000
ADDRESS_MASK: int = 0b00000011111111111111111111111111
ADDRESS_MODE_MASK: int = 0b00001100000000000000000000000000
WORD_SIZE: int = 32
ADDRESS_SIZE: int = 26
ADDRESS_LIMIT: int = 2 ** 24


class Operation:
    def __init__(self, code: int, need_argument: bool):
        self.code: int = code
        self.need_argument: bool = need_argument


class Command(Operation, Enum):
    ADD = 0b0001, True
    LD = 0b0010, True
    INC = 0b0011, False
    DEC = 0b0100, False
    CMP = 0b0101, True
    JUMP = 0b0111, True
    BEQ = 0b1000, True
    BNE = 0b1001, True
    BMI = 0b1010, True
    BPL = 0b1011, True
    OUT = 0b1100, False
    COUT = 0b1101, False
    ST = 0b1110, True
    SUB = 0b1111, True
    HALT = 0b0000, False
    MUL = 0b0110, True


class AddrMode(IntEnum):
    STRAIGHT_LOAD = 0b00000000000000000000000000000000
    STRAIGHT_ABSOLUTE = 0b00000100000000000000000000000000
    INDIRECT_ABSOLUTE = 0b00001000000000000000000000000000
    ADDR_REFERENCE = 0b00001100000000000000000000000000


command_mapper = {
    0b0001: Command.ADD,
    0b0010: Command.LD,
    0b0011: Command.INC,
    0b0100: Command.DEC,
    0b0101: Command.CMP,
    0b0110: Command.MUL,
    0b0111: Command.JUMP,
    0b1000: Command.BEQ,
    0b1001: Command.BNE,
    0b1010: Command.BMI,
    0b1011: Command.BPL,
    0b1100: Command.OUT,
    0b1101: Command.COUT,
    0b1110: Command.ST,
    0b1111: Command.SUB,
    0b0000: Command.HALT
}


class HaltProgram(Exception):
    def __init__(self, message="Processor stopped"):
        self.message = message
        super().__init__(self.message)


def twos_complement(number):
    mask = (1 << WORD_SIZE) - 1
    if number < 0:
        number = ((abs(number) ^ mask) + 1)
    return bin(number & mask)
