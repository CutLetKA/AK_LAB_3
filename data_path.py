# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=line-too-long
# pylint: disable=too-many-instance-attributes

"""
DataPath module, part of processor that gets controlled by ControlUnit
"""

from property import ADDRESS_LIMIT, ADDRESS_MASK, ADDRESS_SIZE


class DataPath:
    def __init__(self, code: list, data: list):
        assert len(code) < ADDRESS_LIMIT, "Program code is larger than available address space"
        assert len(data) < ADDRESS_LIMIT, "Program data is larger than available address space"
        self.code: list = code
        self.data: list = data

        self.accumulator: int = 0
        self.data_register: int = 0
        self.code_register: int = 0
        self.address_register: int = 0

        self.negative_flag: int = 0
        self.zero_flag: int = 0

    def get_command(self, command_addr: int):
        assert command_addr.bit_length() <= ADDRESS_SIZE and command_addr < len(self.code), \
            "Code address overflow"
        self.code_register: int = int(self.code[command_addr], 2)

    def access_data(self, data_addr: int):
        assert data_addr.bit_length() <= ADDRESS_SIZE and data_addr < len(self.data), \
            "Data address overflow"
        self.data_register: int = int(self.data[data_addr], 2)

    def save_data(self, data_addr: int):
        assert data_addr.bit_length() <= ADDRESS_SIZE and data_addr < len(self.data), \
            "Data address overflow"
        self.data[data_addr] = str(bin(self.accumulator)[2:])

    def get_command_arg(self, command_addr: int):
        return int(self.code[command_addr], 2) & ADDRESS_MASK

    def set_flags(self):
        self.negative_flag = 1 if self.accumulator < 0 else 0
        self.zero_flag = 1 if self.accumulator == 0 else 0
