"""
Python realization of prob2
"""

NUM_1: int = 1      # первое число последовательности Фибоначчи
NUM_2: int = 1      # второе число --//--
NUM_3: int = 0      # третье
ACC: int = 0
EVEN: int = 2       # счетчик четных чисел (0 - неч.,1 - неч.,2 - чет.)
RESULT: int = 0     # тут сумму считаем
while True:
    ACC = NUM_1 + NUM_2
    if ACC > 4000000:  # если перешли 4000000 -> на выход
        break
    NUM_1 = NUM_2
    NUM_2 = ACC
    EVEN += 1
    if EVEN == 3:
        EVEN = 0
        RESULT += NUM_2
print(RESULT)
