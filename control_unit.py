# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-statements
# pylint: disable=consider-using-f-string

"""
Module of control unit, processor can be started there
"""
import logging
import sys

from data_path import DataPath
from property import Command, HaltProgram, ADDRESS_MASK, command_mapper,\
    ADDRESS_MODE_MASK, WORD_SIZE, AddrMode
from translator import read_bin

logging.basicConfig(level=logging.INFO)


class ControlUnit:

    def __init__(self, data_path: DataPath, program_counter: int = 0, instr_limit: int = 1000000):
        self.data_path: DataPath = data_path
        self._log_header_empty: bool = True
        self._tick: int = 0
        self._instr_counter: int = 0
        self.instr_limit: int = instr_limit
        self._program_counter: int = program_counter
        self.command_code: int = 0
        self.output: list = []

    def tick(self) -> None:
        self._tick += 1

    def increment_program_counter(self) -> None:
        self._program_counter += 1
        assert self._program_counter <= len(self.data_path.code), \
            "Program counter overflow"

    def set_command_code(self, value: int):
        assert value.bit_length() <= 4, \
            "Command register value overflow"
        assert value in command_mapper, "Unknown command"
        self.command_code = value

    def get_tick(self):
        return self._tick

    def start(self):
        self._program_counter: int = 0
        self._instr_counter: int = 0

        try:
            while True:
                self._instr_counter += 1
                assert self._instr_counter < self.instr_limit, \
                    "Reached limit"
                self.tick()
                self.data_path.get_command(self._program_counter)
                self.handle_command(self.data_path.code_register)
                self.instr_log(self.data_path.code_register)
        except HaltProgram:
            self.instr_log(self.data_path.code_register)

        return ''.join(self.output)

    def handle_command(self, command_code: int):
        self.increment_program_counter()
        self.set_command_code(command_code >> ADDRESS_MODE_MASK.bit_length())

        self.data_path.address_register = command_code & ADDRESS_MASK \
            if command_mapper[self.command_code].need_argument else None

        if command_mapper[self.command_code].need_argument:
            match command_code & ADDRESS_MODE_MASK:

                case AddrMode.STRAIGHT_LOAD | AddrMode.ADDR_REFERENCE:
                    self.tick()
                    self.data_path.data_register = int(self.data_path.address_register)

                case AddrMode.STRAIGHT_ABSOLUTE:
                    self.tick()
                    self.tick()
                    self.data_path.access_data(self.data_path.address_register)

                case AddrMode.INDIRECT_ABSOLUTE:
                    self.tick()
                    self.data_path.access_data(self.data_path.address_register)
                    self.data_path.access_data(self.data_path.data_register)

        match command_mapper[self.command_code]:

            case Command.ADD:
                self.tick()
                self.data_path.accumulator += int(self.data_path.data_register)
                self.data_path.set_flags()

            case Command.SUB:
                self.tick()
                self.data_path.accumulator -= int(self.data_path.data_register)
                self.data_path.set_flags()

            case Command.LD:
                self.tick()
                self.data_path.accumulator = int(self.data_path.data_register)
                self.data_path.set_flags()

            case Command.ST:
                self.tick()
                self.data_path.save_data(self.data_path.address_register)

            case Command.INC:
                self.tick()
                self.data_path.accumulator += 1
                self.data_path.set_flags()

            case Command.DEC:
                self.tick()
                self.data_path.accumulator -= 1
                self.data_path.set_flags()

            case Command.MUL:
                self.tick()
                self.data_path.accumulator *= int(self.data_path.data_register)
                self.data_path.set_flags()

            case Command.CMP:
                self.tick()
                if self.data_path.accumulator == self.data_path.data_register:
                    self.data_path.zero_flag = True
                    self.data_path.negative_flag = False
                elif self.data_path.accumulator > self.data_path.data_register:
                    self.data_path.zero_flag = False
                    self.data_path.negative_flag = False
                else:
                    self.data_path.zero_flag = False
                    self.data_path.negative_flag = True

            case Command.JUMP:
                self.tick()
                self._program_counter = self.data_path.address_register

            case Command.BEQ:
                self.tick()
                if self.data_path.zero_flag:
                    self._program_counter = self.data_path.data_register

            case Command.BNE:
                self.tick()
                if not self.data_path.zero_flag:
                    self._program_counter = self.data_path.address_register

            case Command.BMI:
                self.tick()
                if self.data_path.negative_flag:
                    self._program_counter = self.data_path.address_register

            case Command.BPL:
                self.tick()
                if not self.data_path.negative_flag:
                    self._program_counter = self.data_path.address_register

            case Command.COUT:
                self.tick()
                self.output.append(chr(self.data_path.accumulator))

            case Command.OUT:
                self.tick()
                self.output.append(str(self.data_path.accumulator))

            case Command.HALT:
                self.tick()
                raise HaltProgram

    def instr_log(self, instruction_code: int):

        if self._log_header_empty:
            header: str = '\t%33s\t%4s %8s %6s %4s %6s %8s %8s %8s' % ('Code (CR)',
                                                                       'Desc',
                                                                       'Arg',
                                                                       'Tick',
                                                                       'PC',
                                                                       'CC',
                                                                       'AC',
                                                                       'DR',
                                                                       'IC')
            logging.info('%s', header)
            self._log_header_empty = False

        log_line: str = '\t%s:\t%4s %8s %6s %4s %6s %8s %8s %8s' \
                        % (bin(self.data_path.code_register)[2:].zfill(WORD_SIZE),
                           command_mapper[self.command_code].name,
                           instruction_code & ADDRESS_MASK
                           if command_mapper[self.command_code].need_argument
                           else "-",
                           self._tick,
                           self._program_counter,
                           bin(self.command_code)[2:].zfill(4),
                           self.data_path.accumulator,
                           self.data_path.data_register,
                           self._instr_counter)
        logging.info('%s', log_line)


def run_processor(code: list, data: list) -> str:
    data_path: DataPath = DataPath(code=code, data=data)
    control_unit: ControlUnit = ControlUnit(data_path=data_path)
    output: str = control_unit.start()
    return output


def main(args):
    assert len(args) == 2, \
        "Wrong arguments: control_unit.py <code_file> <data_file>"
    code_target, data_target = args
    code: list[str] = read_bin(code_target)
    data: list[str] = read_bin(data_target)

    output: str = run_processor(code=code, data=data)

    print(output)


if __name__ == '__main__':
    main(sys.argv[1:])
