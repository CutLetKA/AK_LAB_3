# pylint: disable=missing-class-docstring
# pylint: disable=missing-function-docstring
# pylint: disable=too-many-locals
# pylint: disable=too-many-branches
# pylint: disable=too-many-statements

"""
Module of translation .asm code into binary code and data files
"""
import pickle
import sys
import logging

from property import Command, twos_complement, WORD_SIZE, ADDRESS_MODE_MASK, ADDRESS_SIZE, AddrMode

logging.basicConfig(level=logging.DEBUG)

command_mapping = {
    "halt": Command.HALT,
    "add": Command.ADD,
    "ld": Command.LD,
    "inc": Command.INC,
    "dec": Command.DEC,
    "cmp": Command.CMP,
    "jump": Command.JUMP,
    "beq": Command.BEQ,
    "bne": Command.BNE,
    "bmi": Command.BMI,
    "bpl": Command.BPL,
    "cout": Command.COUT,
    "out": Command.OUT,
    "st": Command.ST,
    "sub": Command.SUB,
    "mul": Command.MUL
}


def gel_labels(source_code: list) -> dict:
    operation_counter: int = 0
    code_segment: bool = False
    labels: dict = {}

    for line in source_code:
        words: list[str] = line.split()
        if len(words) == 0:
            continue
        if words[0] == '.code':
            code_segment = True
            continue
        if code_segment:
            if words[0] == '' or words[0][0] == ';':
                continue
            if words[0][-1] == ':':
                label_name: str = words[0][:-1]
                assert label_name not in labels, f"Label {label_name} already defined"
                labels[label_name]: int = operation_counter
                words.pop(0)
            if len(words) > 0 and words[0].lower() in command_mapping:
                operation_counter += 1

        else:
            continue
    return labels


def translate(source) -> tuple[list, list, list]:
    with open(source, "r", encoding="utf-8") as source_file:
        source_code: list[str] = source_file.readlines()
        data_segment: bool = False
        code_segment: bool = False

        data: list = []
        code: list = []
        mnemonics: list = []
        variables: dict = {}

        labels: dict = gel_labels(source_code)

        for line in source_code:
            words: list[str] = line.split()
            for value in words:
                if value[0][0] == ';':
                    continue
                if value == '.data':
                    assert not data_segment, "Data segment already defined"
                    data_segment = True
                    continue
                if value == '.code':
                    assert not code_segment, "Code segment already defined"
                    data_segment = False
                    code_segment = True
                    continue

            if data_segment:
                if len(words) >= 2 and words[0][-1] == ":":
                    var_name: str = words[0][:-1]
                    var_value: str = words[1]
                    assert var_name not in variables, \
                        f"Variable with name {var_name} already defined"
                    try:
                        var_value: int = int(var_value)
                    except ValueError as exc:
                        if len(var_value) == 1:
                            var_value: int = ord(var_value)
                        else:
                            raise ValueError(f"Invalid variable value: {var_name}") from exc

                    var_value: int = int(var_value)
                    data.append(var_value)
                    variables[var_name] = len(data) - 1
                elif len(words) == 1:
                    if words[0][0] != ';' and words[0] != '.data':
                        value: str = words[0]
                        try:
                            value: int = int(value)
                        except ValueError as exc:
                            if len(value) == 1:
                                value: int = ord(value)
                            else:
                                raise ValueError(f"Invalid data value: {value}") from exc
                        data.append(value)

            if code_segment:

                if len(words) == 0 or words[0] == '' or words[0][0] == ';' or words[0] == '.code':
                    continue

                if words[0][-1] == ':':
                    words.pop(0)
                    if len(words) == 0:
                        continue

                mnemonic: str = words[0].lower()

                if command_mapping[mnemonic].need_argument:
                    assert len(words) > 1, f"Command {mnemonic} requires argument"
                    argument: str = words[1]

                    if argument[0] == '#':
                        argument: str = argument[1:]
                        try:
                            value: int = int(argument)
                            code.append(
                                (command_mapping[mnemonic]
                                 .code << ADDRESS_MODE_MASK.bit_length()) + value
                            )
                            continue
                        except ValueError:
                            pass

                        assert argument in variables, f"Unknown variable name: {argument}"
                        address: int = variables[argument] + AddrMode.STRAIGHT_ABSOLUTE

                    elif argument[0] == '$':
                        argument: str = argument[1:]
                        try:
                            argument_value: int = int(argument)
                            address: int = argument_value
                            assert address <= (1 << ADDRESS_SIZE), "Address overflow"
                            address += AddrMode.STRAIGHT_ABSOLUTE
                        except ValueError:
                            assert argument in variables, f"Unknown variable name: {argument}"
                            address: int = variables[argument] + AddrMode.INDIRECT_ABSOLUTE

                    elif argument in labels:
                        address: int = labels[argument]

                    elif argument in variables:
                        address: int = int(variables[argument]) + AddrMode.ADDR_REFERENCE

                    code.append(
                        (command_mapping[mnemonic].code << ADDRESS_MODE_MASK.bit_length()) + address
                    )
                    mnemonics.append(mnemonic + " " + str(address))
                else:
                    code.append(command_mapping[mnemonic].code << ADDRESS_MODE_MASK.bit_length())
                    mnemonics.append(mnemonic)

        return code, data, mnemonics


def write_bin(filename: str, data_list: list) -> None:
    with open(filename, "wb") as binary_file:
        for line in data_list:
            pickle.dump("command", binary_file)
            pickle.dump(twos_complement(line)[2:].zfill(WORD_SIZE), binary_file)
        pickle.dump("end", binary_file)


def read_bin(filename: str) -> list[str]:
    commands: list[str] = []
    with open(filename, "rb") as binary_file:
        flag: str = pickle.load(binary_file)
        while flag == "command":
            commands.append(pickle.load(binary_file))
            flag: str = pickle.load(binary_file)
    return commands


def main(args):
    assert len(args) == 3, \
        "Wrong arguments: translator.py <input_file> <code_target_file> <data_target_file>"
    source, code_target, data_target = args
    code, data, mnemonics = translate(source)

    write_bin(code_target, code)
    write_bin(data_target, data)
    for line in mnemonics:
        logging.info(line)


if __name__ == '__main__':
    main(sys.argv[1:])
