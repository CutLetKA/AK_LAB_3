.data
    result: 0
    num_1:  1
    num_2:  1
    num_3:  0
    even:   2

.code
    start:
            ld  #num_1
            add #num_2
            cmp #4000000
            bpl out
            st  #num_3
            ld  #num_2
            st  #num_1
            ld  #num_3
            st  #num_2
            ld  #even
            inc
            st  #even
            cmp #3
            bpl inc_result
            jump start

    inc_result:
            mul #0
            st  #even
            ld  #result
            add #num_2
            st  #result
            jump start

    out:
            ld  #result
            out
            halt