.data
    f
    o
    o
    32
    o
    u
    t
    p
    u
    t
    stop: -1
    counter: 0
.code
start:
        ld #counter
        cmp stop
        beq end
        ld $counter
        cout
        ld #counter
        inc
        st #counter
        jump start
end:    HALT